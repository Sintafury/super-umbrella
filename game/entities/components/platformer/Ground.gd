extends "res://entities/components/platformer/PhysicsBase.gd"

func _requests_control(entity):
	return entity.is_on_floor()
